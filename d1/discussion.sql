-- MySQL CRUD Operations

-- [SECTION] Inserting A Record

-- SYNTAX: INSERT INTO table_name (column_nameA) VALUES (ValueA);
-- Artist Table
INSERT INTO artists (name) VALUES ("Psy");
INSERT INTO artists (name) VALUES ("Rivermaya");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-07-15", 1);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-02-14", 2);
-- HH:MM:SS -> HHMMSS
-- MySQL allows to use the 'HHMMSS' format without delimiter(:) to represent time value for TIME formats only. (Original format: HH:MM:SS)
	-- But make sure that it still follow the correct max value for each time intervals:
		-- HH = 24
		-- MM = 59
		-- SS = 59
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 339, "K-Pop", 1);

-- Mini-Activity:
	-- insert the following songs in the songs table:

		-- Song name: Kundiman
		-- Length: 5 mins 24 secs
		-- Genre: OPM
		-- Album: Trip

		-- Song name: Kisapmata
		-- Length: 4 mins 41 secs
		-- Genre: OPM
		-- Album: Trip

	-- Check if the songs are added.
	-- send your songs table in the batch hangouts

-- Solution
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kundiman", 524, "OPM", 2);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 441, "OPM", 2);

-- [SECTION] Selecting Records
-- Syntax:
	-- SELECT * FROM table_name; (shows the full table)
		-- (*) means all columns will be shown in the selected table.
	-- SELECT column_nameA, column_nameB FROM table_name;

SELECT * FROM songs;

-- Display the title and genre of all the songs

SELECT song_name, genre FROM songs;

-- Display album title and date released of all the albums.
SELECT album_title, date_released FROM albums;

-- Display the title of all the OPM songs.
-- "WHERE" clause is used to filter records and to extract only those records that fulfill a specific condition.

SELECT song_name FROM songs	WHERE genre = "OPM";

-- AND and OR Keyword
	-- is used for multiple expressions in the WHERE clause.
-- Display the title and length of the OPM Songs that are more than 4 minutes 30 secs.
SELECT song_name, length FROM songs WHERE length > 430 AND genre = "OPM";

-- [SECTION] Updating Records
-- Syntax:
	-- UPDATE table_name SET column_nameA = updated_valueA WHERE condition;

-- Updated the length of Kundiman to 4 minutes 24 secs.
UPDATE songs SET length = 424 WHERE song_name = "Kundiman";

-- [SECTION] Deleting Records
-- Syntax: 
	-- DELETE FROM table_name WHERE condition;
		-- NOTE: Removing the WHERE clause will remove all rows in the table.

-- Delete all OPM songs that are more than 4 minutes 30 seconds.
DELETE FROM songs WHERE genre = "OPM" AND length > 430;